import React, { useState } from 'react';
import styles from '../../styles/UserCard.module.css';
import { Subscription, User } from '../../types/types';
import SubscriptionModal from '../SubscriptionModal';
import { FiMapPin } from 'react-icons/fi';

interface UserCardProps {
  user: User;
  subscriptions: Subscription[];
}

const UserCard: React.FC<UserCardProps> = ({ user, subscriptions }) => {
  const [isModalOpen, setIsModalOpen] = useState(false);

  const userSubscriptions = subscriptions.filter(sub => sub.user_id === user.id.toString());

  const getAvatarInitials = (firstName: string, lastName: string) => {
    return firstName.charAt(0) + lastName.charAt(0);
  };

  const isUserOnline = () => {
    return user.active === '1';
  };

  return (
    <div className={styles.userCard}>
      <div className={styles.avatar}>
        {getAvatarInitials(user.first_name, user.last_name)}
        <div
          className={styles.onlineIndicator}
          style={{ backgroundColor: isUserOnline() ? '#FFBF00' : '#00C49F' }}
        />
      </div>
      <div className={styles.userInfo}>
        <div className={styles.userNameRow}>
          <h3>{user.first_name} {user.last_name}</h3>
        </div>
        <p className={styles.userEmail}>{user.email}</p>
        <div className={styles.location}>
          <FiMapPin />
          <span>{user.address}, {user.country}</span>
        </div>
        <button className={styles.viewSubButton} onClick={() => setIsModalOpen(true)}>View Subscriptions</button>
      </div>
      {isModalOpen && (
        <SubscriptionModal
          subscriptions={userSubscriptions}
          onClose={() => setIsModalOpen(false)}
        />
      )}
    </div>
  );
};

export default UserCard;
