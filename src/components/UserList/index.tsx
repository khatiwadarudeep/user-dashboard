import React, { useState } from 'react';
import styles from '../../styles/UserList.module.css';
import { FaSortAlphaDown, FaSortAlphaUp, FaSearch } from 'react-icons/fa';
import { Subscription, User } from '../../types/types';
import UserCard from '../UserCard';

interface UserListProps {
  users: User[];
  subscriptions: Subscription[];
  loading: boolean
}

const UserList: React.FC<UserListProps> = ({ users, subscriptions }) => {
  const [sortKey, setSortKey] = useState<keyof User | null>(null);
  const [sortOrder, setSortOrder] = useState<'asc' | 'desc'>('asc');
  const [searchTerm, setSearchTerm] = useState<string>('');

  const handleSort = (key: keyof User) => {
    setSortKey(key);
    setSortOrder(sortOrder === 'asc' ? 'desc' : 'asc');
  };

  const sortedUsers = [...users]
    .filter(user =>
      `${user.first_name} ${user.last_name}`.toLowerCase().includes(searchTerm.toLowerCase())
    )
    .sort((a, b) => {
      if (!sortKey) return 0;
      if (sortOrder === 'asc') {
        return a[sortKey]! > b[sortKey]! ? 1 : -1;
      } else {
        return a[sortKey]! < b[sortKey]! ? 1 : -1;
      }
    });

  return (
    <div className={styles.userList}>
      <div className={styles.controls}>
        <div className={styles.sortButtons}>
          <button onClick={() => handleSort('first_name')}>
            Sort by First Name {sortKey === 'first_name' && (sortOrder === 'asc' ? <FaSortAlphaDown /> : <FaSortAlphaUp />)}
          </button>
          <button onClick={() => handleSort('country')}>
            Sort by Country {sortKey === 'country' && (sortOrder === 'asc' ? <FaSortAlphaDown /> : <FaSortAlphaUp />)}
          </button>
        </div>
        <div className={styles.search}>
          <FaSearch />
          <input
            type="text"
            placeholder="Search by name"
            value={searchTerm}
            onChange={(e) => setSearchTerm(e.target.value)}
          />
        </div>
      </div>
      <div className={styles.grid}>
        {sortedUsers.map(user => (
          <UserCard key={user.id} user={user} subscriptions={subscriptions} />
        ))}
      </div>
    </div>
  );
};

export default UserList;
