import React from 'react';
import styles from '../../styles/SubscriptionModal.module.css';
import { Subscription } from '../../types/types';
import { FaTimes,  FaRegListAlt } from 'react-icons/fa';
interface SubscriptionModalProps {
  subscriptions: Subscription[];
  onClose: () => void;
}

const SubscriptionModal: React.FC<SubscriptionModalProps> = ({ subscriptions, onClose }) => {
  return (
    <div className={styles.modal}>
      <div className={styles.modalContent}>
        <div className={styles.header}>
          <h2>Subscription Details</h2>
          <FaTimes className={styles.closeIcon} onClick={onClose} /> {/* Cross icon */}
        </div>
        {subscriptions.length === 0 ? (
          <div className={styles.noSubscription}>No subscription details</div>
        ) : (
          <div className={styles.subscriptionContainer}>
            {subscriptions.map(sub => (
              <div key={sub.id} className={styles.subscription}>
                <div className={styles.package}>
                  <FaRegListAlt className={styles.icon} />
                  {sub.package}
                </div>
                <div className={styles.expiryDate}>
                  <span>Expires on:</span>
                  <span className={styles.date}>
                    {new Date(sub.expires_on).toLocaleDateString()}
                  </span>
                </div>
              </div>
            ))}
          </div>
        )}
      </div>
    </div>
  );
};

export default SubscriptionModal;
