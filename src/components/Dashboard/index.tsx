import React from 'react';
import styles from '../../styles/Dashboard.module.css';
import { Subscription, User } from '../../types/types';
import UserList from '../UserList';
import GraphicalSummary from '../GraphicalSummary';
import { ClipLoader } from 'react-spinners';

interface DashboardProps {
  users: User[];
  subscriptions: Subscription[];
  loading: boolean;
}

const Dashboard: React.FC<DashboardProps> = ({ users, subscriptions,loading }) => {
  return (
    <div className={styles.dashboard}>
      <h1>Subscriber Dashboard</h1>
      {loading ? (
        <div className={styles.spinnerContainer}>
          <ClipLoader color="#D10363" loading={loading} size={150} />
        </div>
      ): (
        <>
      <GraphicalSummary users={users} subscriptions={subscriptions} loading={loading}/>
      <UserList users={users} subscriptions={subscriptions} loading={loading}/>
      </>
      )}
    </div>
  );
};

export default Dashboard;
