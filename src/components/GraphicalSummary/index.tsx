import React from 'react';
import { PieChart, Pie, Cell, Tooltip, ResponsiveContainer } from 'recharts';
import styles from '../../styles/GraphicalSummary.module.css';
import { Subscription, User } from '../../types/types';

interface GraphicalSummaryProps {
  users: User[];
  subscriptions: Subscription[];
  loading: boolean;
}

const GraphicalSummary: React.FC<GraphicalSummaryProps> = ({ users, subscriptions }) => {
  const userData = [
    { name: 'Active', value: users.filter(user => user.active === '1').length },
    { name: 'Inactive', value: users.filter(user => user.active === '0').length },
  ];

  const packageCounts: { [key: string]: number } = {
    'Plan 1': 0,
    'Plan 2': 0,
    'Plan3': 0,
    'Plan 6': 0,
    'Plan 12': 0,
    'Plan Unlimited': 0,
  };

  subscriptions.forEach(sub => {
    if (packageCounts.hasOwnProperty(sub.package)) {
      packageCounts[sub.package]++;
    }
  });

  const subscriptionData = Object.keys(packageCounts).map(key => ({
    name: key,
    value: packageCounts[key],
  }));

  const COLORS = ['#808836', '#FFBF00', '#FF9A00', '#D10363', '#FF5A5F', '#FF6384'];

  return (
    <div className={styles.graphicalSummary}>
      <div className={styles.chartWrapper}>
        <div className={styles.chartContainer}>
          <h3 style={{textAlign:"left"}}>User Status</h3>
          <div className={styles.chartContent}>
            <ResponsiveContainer className={styles.container}>
              <PieChart>
                <Pie data={userData} dataKey="value" nameKey="name" outerRadius={100} fill="#8884d8">
                  {userData.map((_, index) => (
                    <Cell key={`cell-${index}`} fill={index=== 0? '#FFBF00':'#00C49F' } />
                  ))}
                </Pie>
                <Tooltip />
              </PieChart>
            </ResponsiveContainer>
            <div className={styles.info}>
              {userData.map((entry, index) => (
                <div key={`info-${index}`} className={styles.infoItem}>
                  <span
                    className={styles.legendColor}
                    style={{ backgroundColor: index=== 0? '#FFBF00':'#00C49F' }}
                  />
                  <span>{entry.value} {entry.name} Users </span>
                </div>
              ))}
            </div>
          </div>
        </div>
        <div className={styles.chartContainer}>
          <h3>Subscription Packages</h3>
          <div className={styles.chartContent}>
            <ResponsiveContainer className={styles.container} >
              <PieChart>
                <Pie
                  data={subscriptionData}
                  dataKey="value"
                  nameKey="name"
                  outerRadius={100}
                  fill="#8884d8"
                >
                  {subscriptionData.map((_, index) => (
                    <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />
                  ))}
                </Pie>
                <Tooltip />
              </PieChart>
            </ResponsiveContainer>
            <div className={styles.info}>
              {subscriptionData.map((entry, index) => (
                <div key={`info-${index}`} className={styles.infoItem}>
                  <span
                    className={styles.legendColor}
                    style={{ backgroundColor: COLORS[index % COLORS.length] }}
                  />
                  <span>{entry.name}: {entry.value}</span>
                </div>
              ))}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default GraphicalSummary;
