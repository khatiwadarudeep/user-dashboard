import { useState, useEffect } from 'react';
import userData from '../assets/data/users.json';
import subscriptionData from '../assets/data/subscriptions.json';

export const useAsyncData = <T,>(initialData: T[], dataType: 'users' | 'subscriptions') => {
  const [data, setData] = useState<T[]>(initialData);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    setTimeout(() => {
      if (dataType === 'users') {
        setData(userData as T[]);
      } else if (dataType === 'subscriptions') {
        setData(subscriptionData as T[]);
      }
      setLoading(false);
    }, 1000);
  }, [dataType]);

  return { data, loading };
};
