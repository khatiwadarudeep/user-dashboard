import React from 'react';
import { useAsyncData } from './hooks/useAsyncData';
import { Subscription, User } from './types/types';
import Dashboard from './components/Dashboard';

const App: React.FC = () => {
  const { data: users, loading: usersLoading } = useAsyncData<User>([], 'users');
  const { data: subscriptions, loading: subscriptionsLoading } = useAsyncData<Subscription>([], 'subscriptions');
  return (
    <div>
      <Dashboard users={users} subscriptions={subscriptions} loading={usersLoading|| subscriptionsLoading}/>
    </div>
  );
};

export default App;
